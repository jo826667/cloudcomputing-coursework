package prototype;

import java.util.Date;
import java.text.SimpleDateFormat;


public class FlightPassenger {
	
	private String passengerID;
	private String flightID;
	private String airportDep;
	private String airportAri;
	private String departureUnix;
	private String departureString;
	private Date departureTime;
	private Date arrivalTimereal;
	private String arrivalTimerealstring;
	private int flightTime;
	 

	
	public FlightPassenger(String passengerID, String flightID, String dep, String ari, String departureUnix, String flightTime) {
		this.passengerID = passengerID;
		this.flightID = flightID;
		
		this.airportDep = dep;
		this.airportAri = ari;
		
		this.departureUnix = departureUnix;
		this.departureTime = new Date(Long.valueOf(departureUnix)*1000);
		this.departureString = ConvertDate(departureTime);
		this.flightTime = Integer.parseInt(flightTime);
		long arrrivalMili = this.departureTime.getTime() + ((this.flightTime * 60) *1000);
		this.arrivalTimereal = new Date(arrrivalMili);
		this.arrivalTimerealstring = ConvertDate(arrivalTimereal);
	}
	
	//get function
	public String getPassengerID(){
		return passengerID;
	}
	
	public String getFlightID(){
		return flightID;
	}
	
	public String getDep(){
		return airportDep;
	}
	
	
	public String getArr(){
		return airportAri;
	}
	
	//convert to string with passenger ID
	public String toString(){
		return passengerID ;
	}
	
	//convert to string with info in task2
	public String toString1(){
		return "Departure airport: " + airportDep + ", Arrival airport: " + airportAri + ", departure time: " + departureString + ", flight time: " + flightTime + " mins, Arrival Time: "+arrivalTimerealstring+"\nPassenger list:";
	}
	
	//convert input Unix epoch time to "hh::mm:ss" version
	public String ConvertDate(Date date){
		SimpleDateFormat dat = new SimpleDateFormat("hh:mm:ss"); 
		String convertedDate = dat.format(date);
		return convertedDate;
	}
	
	
}
