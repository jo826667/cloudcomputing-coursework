package prototype;

public class Airport {
	private String aircode;
	private double lat;
	private double lon;
	
	//constructor
	Airport(String code, String lat, String lon){
		this.aircode = code;
		this.lat = Double.parseDouble(lat);
		this.lon = Double.parseDouble(lon);
	}
	
	//get function
	public String getCode(){
		return aircode;
	}
	
	public double getLat(){
		return lat;
	}
	
	public double getLon(){
		return lon;
	}
	
	//to string
	public String toString(){
		return aircode + " lat: " + lat + " lon: " + lon;
	}
	
	
	
}
