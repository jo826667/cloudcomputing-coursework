package prototype;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;


public class MainFunction {
	
//write output in file, and print the result in console 
	
	public static void record(String word, FileWriter file){
		try {
			file.write(word);
			System.out.print(word);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
//set up "Mappers"
	static TreeMap<Integer, FlightPassenger> flightPassengerInput = new TreeMap<Integer, FlightPassenger>();
	static TreeMap<String, List<FlightPassenger>> flightPassenger = new TreeMap<String, List<FlightPassenger>>();
	static TreeMap<Integer, Airport> airportsInput = new TreeMap<Integer, Airport>();
	static TreeMap<String, Airport> airport = new TreeMap<String, Airport>();
    static ArrayList<String> allairportlist = new ArrayList() ;
	//read in "Top30_airports_LatLon.csv", error detection and started map phase
    public static void readinairport() {
		try {
		BufferedReader reader = new BufferedReader(new FileReader(inputAirport));
		String row;
		//the position at the original dataset
		int datarowpos1 = 1;
		//position at the map
		int pos = 0;
		while ((row = reader.readLine()) != null) {
			String[] cut = row.split(",");
			if (cut.length == 4) {
				try {
					if (cut[1].matches("[A-Z]{3}") & cut[2].matches("[+-]?\\d*\\.?\\d*") & cut[3].matches("[+-]?\\d*\\.?\\d*")) {
					    airportsInput.put(pos,new Airport(cut[1], cut[2], cut[3]));
						pos++;
						allairportlist.add(cut[1]);
					 }
					if (!cut[1].matches("[A-Z]{3}")) {record("Airport dataset: Airport code is wrong in Row "+ datarowpos1 +"\n",errorfile);}
					if (!cut[2].matches("[+-]?\\d*\\.?\\d*")) {record("Airport dataset: Longtitude is wrong in Row "+ datarowpos1 +"\n",errorfile);}
					if (!cut[3].matches("[+-]?\\d*\\.?\\d*")) {record("Airport dataset: Latitude is wrong in Row "+ datarowpos1 +"\n",errorfile);}
				} catch (Exception e) {
					System.out.println("Can not add row" + datarowpos1 + "to the map.");
				}
			} else {record("Airport dataset: Empty row " + datarowpos1 + "\n",errorfile);}
			datarowpos1++;
			
		} 
		reader.close();}
		catch (IOException e) {
			e.printStackTrace();
			}
		}
	//read in AComp_Passenger dataset with error detection and correction and map phase part 1
	public static void readinflight() {
		try {
	    //set up buffer reader to read in file
		BufferedReader reader1 = new BufferedReader(new FileReader(inputFlightPassenger));
		String row;		
		//the position at the original dataset
		int datarowpos = 1;
		//the position in the map
		int pos = 0;
		Set set = new HashSet();
		//read in data through buffer
		while ((row = reader1.readLine()) != null) {
			String[] cut = row.split(",");
			if (cut[0].length() == 0 | cut[1].length() == 0 | cut[2].length() == 0 |cut[3].length() == 0 | cut[4].length() == 0 | cut[5].length() == 0 ) {
				record("Error: Missing values in row " + datarowpos + "\n" ,errorfile);
				} else {
				try {
					//report duplicate and delete the duplicate line 
					if (cut[0].matches("[A-Z]{3}[0-9]{4}[A-Z]{2}[0-9]{1}") & cut[1].matches("[A-Z]{3}[0-9]{4}[A-Z]{1}") & 
							cut[2].matches("[A-Z]{3}") & cut[3].matches("[A-Z]{3}") & cut[4].matches("\\d{10}") &
							cut[5].matches("\\d{1,4}") & allairportlist.contains(cut[2]) & allairportlist.contains(cut[3])) {
						   
							   if(!set.contains(row)) {
					        	 flightPassengerInput.put(pos,new FlightPassenger(cut[0], cut[1], cut[2], cut[3], cut[4], cut[5]));
					        	 pos++;
					        	 set.add(row);
						         
						   		} else {
					        		 record("Error: duplicate row in row" + datarowpos+": " + row + "\n",errorfile);
						   		} 
					}   
					//cross reference error
					if (cut[2].matches("[A-Z]{3}") & !allairportlist.contains(cut[2])) {record("Error: Departure airport is not in the airport list in row "+datarowpos+ " : "+ cut[2]+"\n",errorfile);}
			   		if (cut[3].matches("[A-Z]{3}") & !allairportlist.contains(cut[3])) {record("Error: Arrival airport is not in the airport list in row "+datarowpos+ " : "+ cut[3]+"\n",errorfile);}
					//syntax error
			   		if (!cut[0].matches("[A-Z]{3}[0-9]{4}[A-Z]{2}[0-9]{1}")) {record("Error: syntax error in Passenger ID in row "+ datarowpos + " : "+ cut[0]+"\n",errorfile);} 
					if (!cut[1].matches("[A-Z]{3}[0-9]{4}[A-Z]{1}")) 	{record("Error: syntax error in Flight ID in row "+ datarowpos + " : "+ cut[1]+"\n",errorfile); }	
					if (!cut[2].matches("[A-Z]{3}")) {record("Error: syntax error in Departure airport in row "+ datarowpos + " : "+ cut[2]+"\n",errorfile);}
					//syntax error correction
					if ((!cut[2].matches("[A-Z]{3}")) & cut[0].matches("[A-Z]{3}[0-9]{4}[A-Z]{2}[0-9]{1}") & cut[1].matches("[A-Z]{3}[0-9]{4}[A-Z]{1}") & 
							cut[3].matches("[A-Z]{3}") & cut[4].matches("\\d{10}")) {
							for(int i = 0; i < allairportlist.size(); i++){
							   if(allairportlist.get(i).contains(cut[2].substring(0,2)) | 
									   allairportlist.get(i).contains(cut[2].substring(1,3))){
								       record("Syntax error correction: change departure airport " + cut[2] + " to "+allairportlist.get(i) + "\n",errorfile);
								       if(!set.contains(row)) {
								   	   flightPassengerInput.put(pos,new FlightPassenger(cut[0], cut[1], allairportlist.get(i), cut[3], cut[4], cut[5]));
								   	   pos++;
								   	   set.add(row);
								       }   	else {
						        		 record("After correction, duplicate row in Row"+datarowpos + ": " + row + "\n",errorfile);
							   		}  
								     
								       break;	       
							   }	
						}
					}
					//syntax error
			    	if (!cut[3].matches("[A-Z]{3}")) { record("Error: syntax error in Arrival airport in row "+ datarowpos + " : "+ cut[3]+"\n",errorfile); }	
					//syntax error correction
			    	if ((!cut[3].matches("[A-Z]{3}")) & cut[0].matches("[A-Z]{3}[0-9]{4}[A-Z]{2}[0-9]{1}") & cut[1].matches("[A-Z]{3}[0-9]{4}[A-Z]{1}") & 
							cut[2].matches("[A-Z]{3}") & cut[4].matches("\\d{10}")) {
							for(int i = 0; i < allairportlist.size(); i++){
							   if(allairportlist.get(i).contains(cut[3].substring(0,2)) | 
									   allairportlist.get(i).contains(cut[3].substring(1,3))){
								       record("Syntax error correction: change arrival airport "+cut[3] +" to " + allairportlist.get(i) + "\n",errorfile);
								       if(!set.contains(row)) {
								   	   flightPassengerInput.put(pos,new FlightPassenger(cut[0], cut[1],  cut[2], allairportlist.get(i), cut[4], cut[5]));
								   	   pos++;
								   	   set.add(row);
								       }   	else {
						        		 record("After correction, duplicate row in Row"+datarowpos + ": " + row + "\n",errorfile);
							   		} 
								     
								       break;	       
							   }	
						}
					}
			    	//syntax error
			    	if (!cut[4].matches("\\d{10}")) {record("Error: syntax error in Departure time in row " + datarowpos + " : " + cut[4] + "\n",errorfile); }	
			    	if (!cut[5].matches("\\d{1,4}")) {record("Error: syntax error in Total mins in row " + datarowpos + " : " + cut[5] + "\n",errorfile); } 
				} catch (Exception e) {
					//catch any exception
					e.printStackTrace();
				}
			}  
			datarowpos++;

		}   
		reader1.close();}
		catch (IOException e) {
			e.printStackTrace();
		}
		}
	
	/* Object1 */

	public static void objective1() {
		//map phase part 2 (shared by objective 1 2 3 and 4) (with shuffle)

		TreeMap<String, List<String>> Map1 = new TreeMap<String, List<String>>();
		for (Map.Entry<Integer, FlightPassenger> entry : flightPassengerInput.entrySet()) {
			if (Map1.get(entry.getValue().getDep()) == null) {
				Map1.put(entry.getValue().getDep(), new ArrayList<String>());
			}
			Map1.get(entry.getValue().getDep()).add(entry.getValue().getFlightID());
			
		}
		
		for (Map.Entry<Integer, Airport> entry : airportsInput.entrySet()) {
			if (airport.get(entry.getValue().getCode()) == null) {
				airport.put(entry.getValue().getCode(), entry.getValue());
		    }
		}
		
		//reduce phase for objective 1
		TreeMap<String, Integer> reducer1 = new TreeMap<String, Integer>();
			for (Entry<String, List<String>> entry : Map1.entrySet()) {
				ArrayList<String> uniqueList = (ArrayList<String>) entry.getValue().stream().distinct().collect(Collectors.toList());
                reducer1.put(entry.getKey(),uniqueList.size());}
		//Part1: print the result for the list of the number of flights from each airport 
		record("Output for Objective 1\n",output1);
		record("List of the number of flights from each airport: \n",output1);

		for (Entry<String, Integer> entry : reducer1.entrySet()) {
			record(entry.getKey() + " : " + entry.getValue() + "\n", output1);
		}
			
        //Part 2: output of the unused airport
		ArrayList<String> airportlist = new ArrayList<String> (airport.keySet());
		ArrayList<String> l = new ArrayList<String>(Map1.keySet());
		ArrayList<String> ll= airportlist;
		ArrayList<String> base = new ArrayList<String>(ll);
		base.removeAll(l);
		record("List of unused airport: \n",output1);
		for(String ele: base) {
			record(ele + "\n",output1);
		} 
	}

	/****Objective2****/
	public static void objective2() {
		record("\nOutput for Objective 2\n",output2);
		record("\nList of flights based on the Flight id\n",output2);
	    //Map phase part 2 (with shuffle)
		for (Map.Entry<Integer, FlightPassenger> entry : flightPassengerInput.entrySet()) {
			if (flightPassenger.get(entry.getValue().getFlightID()) == null) {
				flightPassenger.put(entry.getValue().getFlightID(), new ArrayList<FlightPassenger>());
			} 
				flightPassenger.get(entry.getValue().getFlightID()).add(entry.getValue());
			
		}
		
		// Reduce phase 
		Map<String, String> reducer2 = new TreeMap<String, String>();
		for (Map.Entry<String, List<FlightPassenger>> entry : flightPassenger.entrySet()) {
			
			int i = 0;
			String info = "";
			for (FlightPassenger entry2 : entry.getValue()) {
				if (i == 0) {
					info += entry2.toString1();
					info += "\n";
					i++;}
				info += entry2.toString();
				info += "\n";
				}
			reducer2.put(entry.getKey(), info);
		    }
		// Print output for objective2
		for (Map.Entry<String, String> entry : reducer2.entrySet()) {
			record(entry.getKey() + ": \n" + entry.getValue() + "\n", output2);
		} 
	}
	

	/*****Objective3*****/
	
	public static void objective3() {
		//Map phase is the same as in objective2
		//Reduce phase phase
		record("\nOutput for Objective 3\n",output3);
		record("\nList of  the number of passengers on each flight\n",output3);
		
		Map<String, Integer> reducer3 = new TreeMap<String, Integer>();
		for (Map.Entry<String, List<FlightPassenger>> entry : flightPassenger.entrySet()) {
			reducer3.put(entry.getKey(), entry.getValue().size());
		}
		//Print output for objective3
		for (Map.Entry<String, Integer> entry : reducer3.entrySet()) {
			record(entry.getKey() + ": " + entry.getValue() +"\n", output3);
		}
	}

	/*****Objective4*****/
	
	public static void objective4() {
		
		record("\nOutput for Objective 4\n",output4);
		record("\nList of nautical miles for each flight:\n",output4);		
		//record for the passengerID with highest nautical mile records
        String highestMilePassID = "";
        double highestMile = 0;

		//map phase part2 (with shuffle)
		TreeMap<String, Double> NauticalMiles = new TreeMap<String, Double>();
		
		for (Map.Entry<String, List<FlightPassenger>> entry : flightPassenger.entrySet()) {
			for (FlightPassenger entry2 : entry.getValue()) {
				try {
					//calculate the nautical miles
					double lat1 = ((Airport) airport.get(entry2.getDep())).getLat();
					double lon1 = ((Airport) airport.get(entry2.getDep())).getLon();
					double lat2 = ((Airport) airport.get(entry2.getArr())).getLat();
					double lon2 = ((Airport) airport.get(entry2.getArr())).getLon();
				    final int R = 6371; // Radius of the earth
					double latDistance = Math.toRadians(lat2 - lat1);
					double lonDistance = Math.toRadians(lon2 - lon1);
					double num1 = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
					            + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
					            * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
					double num2 = 2 * Math.atan2(Math.sqrt(num1), Math.sqrt(1 - num1));
					double distance = (R * num2) /1.852; // convert to meters
					NauticalMiles.put(entry.getKey(), distance);
				} catch (Exception e) {
					record("Exception: Can not calculate from " + entry2.getDep() + " to "
							+ entry2.getArr() + ".\n", output4);
				}
			}
		}
		// Reduce phase for objective4 part1: List of nautical miles for each flight:
 
		Map<String, Double> reducer4 = new TreeMap<String, Double>();
		for (Map.Entry<String, Double> entry : NauticalMiles.entrySet()) {
			reducer4.put(entry.getKey(), entry.getValue());
		}
		for (Map.Entry<String, Double> entry : reducer4.entrySet()) {
			record(entry.getKey() + ": " + entry.getValue() + " nautical miles.\n", output4);
		}
			
		record("\nTotal nautical miles travelled by each passenger: \n", output4);
		// Reduce phase for objective4 part 2:Total nautical miles travelled by each passenger: 
		TreeMap<String, Double> passengerDistance = new TreeMap<String, Double>();

		for (Map.Entry<String, List<FlightPassenger>> entry : flightPassenger.entrySet()) {
				for(FlightPassenger entry2 : entry.getValue()){
					if (passengerDistance.get(entry2.getPassengerID()) == null) {
						passengerDistance.put(entry2.getPassengerID(), NauticalMiles.get(entry2.getFlightID()));
					} else {
						double distance = passengerDistance.get(entry2.getPassengerID());
						passengerDistance.put(entry2.getPassengerID(), distance + NauticalMiles.get(entry2.getFlightID()));
					}
				}
		}
		
		//Reduce phase for objective4 Part 3: find the Passenger with highest nautical miles.
		for (Map.Entry<String, Double> entry : passengerDistance.entrySet()) {
		     if (entry.getValue() > highestMile) {
		    	 highestMile = entry.getValue();
		    	 highestMilePassID = entry.getKey();
		     }
		}
		
	    //Print the output for part 2 and 3
		for (Map.Entry<String, Double> entry : passengerDistance.entrySet()) {
			record(entry.getKey() + ": " + entry.getValue() + " nautical miles. \n", output4);
		}
        record("\nThe passenger ID having earned the highest air miles\n", output4);		
        record("Passenger ID: "+ highestMilePassID + "Total nautial miles: " +  highestMile, output4);		
     }



	/***Main function***/
	
	static String inputFlightPassenger = "";
	static String inputAirport = "";
	static String outputFile1 = "";
	static String outputFile2 = "";
	static String outputFile3 = "";
	static String outputFile4 = "";
    static String errorFile = "";
    static FileWriter output;
	static FileWriter output1;
	static FileWriter output2;
	static FileWriter output3;
	static FileWriter output4;
    static FileWriter errorfile;

	public static void main(String[] args) throws FileNotFoundException {

		try {
			inputFlightPassenger = "./src/AComp_Passenger_data.csv";
			inputAirport = "./src/Top30_airports_LatLong(1).csv";
			outputFile1 = "./src/output1.csv";
			outputFile2 = "./src/output2.csv";
			outputFile3 = "./src/output3.csv";
			outputFile4 = "./src/output4.csv";
			errorFile = "./src/error.csv";
			errorfile = new FileWriter(errorFile);
			output1 = new FileWriter(outputFile1);
			output2 = new FileWriter(outputFile2);
			output3 = new FileWriter(outputFile3);
			output4 = new FileWriter(outputFile4);

		} catch (Exception e) {
			System.out.println("Input dataset can not found/ Output file can not be found.");
			System.exit(0);
		}

		try {
			readinairport();
			readinflight() ;
			objective1();
			objective2();
			objective3();
			objective4();

			output1.flush();
			output1.close();
			
			output2.flush();
			output2.close();
			
			output3.flush();
			output3.close();
			
			output4.flush();
			output4.close();
			
			errorfile.flush();
			errorfile.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	  }
	}
